let data = {};
const { ipcRenderer } = require('electron')
const remote = require('electron').remote
const thisWin = remote.getCurrentWindow()


setTimeout(function () {
    $('.start-screen-recording-big').trigger("click");
}, 500)


// 监听全屏
thisWin.on('maximize', function () {
    $('#full').hide()
    $('#restore').show()
})

// 监听退出退出全屏
thisWin.on('unmaximize', function () {
    $('#full').show()
    $('#restore').hide()
})

// 最小化
function subtraction() {
    thisWin.minimize()
}

// 全屏
function screenFull() {
    thisWin.maximize()
}

// 退出全屏
function screenRestore() {
    thisWin.restore()
}

// 关闭
function closeX() {
    thisWin.close()
}