//用于控制应用程序生命周期和创建本机浏览器窗口的模块
const { app, BrowserWindow, Menu, Tray, ipcMain, shell, nativeImage } = require('electron')

// 在文件头部引入 Node.js 中的 path 模块
const path = require('path')

const icon = path.join(__dirname, '/icon/icon.ico') // 图标
let win = '';

// 创建窗口
function createWindow() {
    //创建浏览器窗口。
    win = new BrowserWindow({
        width: 800,
        height: 600,
        frame: false,
        show: false,
        // backgroundColor: '#2e2c29',
        webPreferences: {
            preload: path.join(__dirname, '/js/preload.js'),
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true
        },
        icon: icon
    })
    //并加载应用程序的index.html。
    win.loadFile('index.html')

    win.once('ready-to-show', () => {
        win.show()
    })
}

// 系统托盘
let tray = null;

function createTray() {
    tray = new Tray(nativeImage.createFromPath(icon))
    const contextMenu = Menu.buildFromTemplate([
        {
            label: '全屏', click: function () {
                win.maximize()
            }
        },
        { label: 'separator', type: 'separator' },
        {
            label: '最小化', click: function () {
                win.minimize()
            }
        },
        { label: 'separator ', type: 'separator' },
        {
            label: '默认大小', click: function () {
                win.restore()
            }
        },
        { label: 'separator ', type: 'separator' },
        {
            label: '退出', click: function () {
                win.close()
            }
        }
    ])
    tray.setToolTip('屏幕录制')
    tray.setContextMenu(contextMenu)
}

app.whenReady().then(() => {
    createWindow()
    createTray();

    //在macOS上，当应用程序
    //已单击停靠图标，并且没有其他窗口打开。
    app.on('activate', function () {
        if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })
})

// 监听窗体的创建
app.on('web-contents-created', (e, webContents) => {
    webContents.on('new-window', (event, url) => {
        event.preventDefault();
        shell.openExternal(url);
    });
});

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') app.quit()
})