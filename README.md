# Recording 录屏录制

#### 介绍
Recording （录屏录制） ，集成一个小工具 录屏 软件以方便个人使用， 支持  自定义区域 录制、麦克风和系统声音、多种 视频格式、摄像头、录制工具画笔等

#### 软件架构
软件架构说明    
调试、编译、打包 均需要 Node.js（14.16.0）环境支持
1. 请先安装依赖
```
npm install
```

2. 调试运行命令
```
npm run start
```

3.electron-package 打包 先安装electron-packager模块

```
npm install electron-packager --save-dev

electron-packager ./ Recording --platform=win32 --arch=x64 --icon=./icon/icon.ico --out=./out --asar --app-version=0.6.8 --overwrite --ignore=node_modules --electron-version 5.0.0
```

4.打包exe
```
electron-builder --win --x64
```



#### 图片展示

<img src="https://images.gitee.com/uploads/images/2021/0723/184424_427e60b1_3035826.png" width="500" title="开始页面">

<img src="https://images.gitee.com/uploads/images/2021/0723/184517_ae6964f8_3035826.png" width="500" title="启动程序">

<img src="https://images.gitee.com/uploads/images/2021/0723/184706_9189724c_3035826.png" width="500" title="启动程序">

<img src="https://images.gitee.com/uploads/images/2021/0723/184825_e9da2b97_3035826.png" width="500" title="开始录制">

<img src="https://images.gitee.com/uploads/images/2021/0723/185229_5041a3f0_3035826.png" width="500" title="使用画笔">



#### 使用说明

1.  首次打开使用时，会提示下载 启动器 插件
![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/185553_adefa4d8_3035826.png "屏幕截图.png")
2.  将启动器下载安装后，便会自动启动

#### 下载地址
[下载地址](https://gitee.com/starphp/recording/releases/V0.6.8)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 特别鸣谢 （排名不分先后）
1. [傲软在线录屏API](https://www.apowersoft.cn/api/screen-recorder.html)
2. [Node.js](http://nodejs.cn/)
3. [Electron](https://www.electronjs.org/)
4. [Chromium](https://github.com/chromium/chromium)
5. [layui](https://www.layui.com/)

### 版权及免责申明

1.仅允许用于个人学习研究使用, 请勿商用（如有需要请自行联系 [由Apowersoft技术支持](https://www.apowersoft.cn/api/screen-recorder.html)）;

2.录屏相关功能 来着 傲软在线录屏API （[由Apowersoft技术支持](https://www.apowersoft.cn/api/screen-recorder.html)）及 录屏功能版权所有者

3.禁止将本开源的代码和资源进行任何形式任何名义的出售，否则产生的一切任何后果责任由侵权者自负。

4.免责申明：该工具只做简单的集成，且无恶意有害代码；仅限于学习使用，若用于其他用途所产生的一切任何后果责任由使用者自负，与本开源无关且无责